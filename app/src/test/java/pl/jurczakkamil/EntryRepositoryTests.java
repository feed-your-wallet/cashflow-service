package pl.jurczakkamil;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static pl.jurczakkamil.helper.EntryHelper.createEntry;

@SpringBootTest
class EntryRepositoryTests {

    private Validator validator;

    @ParameterizedTest
    @ValueSource(strings = {"1000000000.00", "0.0", "0000000000.00"})
    void givenEntryDtoWithBadAmount_whenValidate_thenReturnViolation_1(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);

        assertEquals(1, violations.size());
    }

    @BeforeEach
    public void setup() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.001", "10.000", "1000000000.00"})
    void givenEntryDtoWithBadAmount_whenValidate_thenReturnViolation_2(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);

        assertEquals("Amount should has max 9 digits before comma and max 2 digits after comma", violations.iterator().next().getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"-10.0", "-1", "-0.1", "0", "0.0", "0.00", "000.00"})
    void givenEntryDtoWithBadAmount_whenValidate_thenReturnViolation_3(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);

        assertEquals("Amount must be a greater than 0.0", violations.iterator().next().getMessage());
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.0000", "0.000", "0000000000.000"})
    void givenEntryDtoWithBadAmount_whenValidate_thenReturnViolation_4(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);

        assertEquals(2L, violations.size());
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.0000", "0.000", "0000000000.000", "0000000000.00"})
    void givenEntryDtoWithBadAmount_whenValidate_thenReturnViolation_5(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);
        final var message = violations.iterator().next().getMessage();

        assertTrue(message.equals("Amount must be a greater than 0.0") ||
                message.equals("Amount should has max 9 digits before comma and max 2 digits after comma"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"0.01", "0.1", "1", "1.21", "999999999", "999999999.9", "999999999.99", "000000001.00"})
    void givenEntryDtoWithCorrectAmount_whenValidate_thenNoViolations(String value) {
        final var entry = createEntry();
        entry.setAmount(new BigDecimal(value));

        final var violations = validator.validate(entry);

        assertEquals(0, violations.size());
    }

    @Test
    void givenBigDecimalWithNullString_whenParseString_thenThrowsEx() {
        assertThrows(NumberFormatException.class,
                () -> new BigDecimal("null"),
                "Character n is neither a decimal digit number, decimal point, nor \"e\" notation exponential mark");
    }
}
