package pl.jurczakkamil;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import pl.jurczakkamil.consts.CurrencyType;
import pl.jurczakkamil.consts.EntryType;
import pl.jurczakkamil.consts.PaymentType;
import pl.jurczakkamil.helper.EntryHelper;

import static org.junit.jupiter.api.Assertions.*;
import static pl.jurczakkamil.helper.EntryHelper.createEntryDto;

@SpringBootTest
class EntryServiceTests {

    @Autowired
    private EntryService entryService;

    @Test
    void contextLoads() {
        assertNotNull(entryService);
    }

    @Test
    void givenEntryDto_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertNotNull(found.get());
    }

    @Test
    void givenEntryDtoWithTimestampUTC_whenSave_thenPersistOkInDb() {
        final var entryDto = createEntryDto();

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        final var date = found.get().getDate();
        final var formatted = EntryHelper.dateFormat.format(date);
        assertEquals("2021-09-02T21:00", formatted);
    }

    @Test
    void givenEntryDtoWithoutType_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(false, true, true, true, true, true, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutAccountId_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, false, true, true, true, true, true, true);

        assertThrows(Exception.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutAmount_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, true, false, true, true, true, true, true);

        assertThrows(Exception.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutCurrency_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, true, true, false, true, true, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutCategoryId_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, true, true, true, false, true, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutDate_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, true, true, true, true, false, true, true);

        assertThrows(DataIntegrityViolationException.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenEntryDtoWithoutDescription_whenSave_thenPersistInDbWithNull() {
        final var entryDto = createEntryDto(true, true, true, true, true, true, false, true);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertNull(found.get().getDescription());
    }

    @Test
    void givenEntryDtoWithoutPaymentType_whenSave_thenThrowsEx() {
        final var entryDto = createEntryDto(true, true, true, true, true, true, true, false);

        assertThrows(DataIntegrityViolationException.class,
                () -> entryService.save(entryDto));
    }

    @Test
    void givenNull_whenSave_thenThrowsEx() {
        assertThrows(NullPointerException.class,
                () -> entryService.save(null),
                "EntryDto cannot be null");
    }

    @Test
    void givenEntryDtoWithTypeEqIncome_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setType(EntryType.INCOME);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(EntryType.INCOME, found.get().getType());
    }

    @Test
    void givenEntryDtoWithTypeEqExpense_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setType(EntryType.EXPENSE);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(EntryType.EXPENSE, found.get().getType());
    }

    @Test
    void givenEntryDtoWithTypeEqTransfer_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setType(EntryType.TRANSFER);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(EntryType.TRANSFER, found.get().getType());
    }

    @Test
    void givenEntryDtoWithCurrencyEqPLN_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setCurrency(CurrencyType.PLN);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(CurrencyType.PLN, found.get().getCurrency());
    }

    @Test
    void givenEntryDtoWithPaymentTypeEqCash_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setPaymentType(PaymentType.CASH);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(PaymentType.CASH, found.get().getPaymentType());
    }

    @Test
    void givenEntryDtoWithPaymentTypeEqCard_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setPaymentType(PaymentType.CARD);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(PaymentType.CARD, found.get().getPaymentType());
    }

    @Test
    void givenEntryDtoWithPaymentTypeEqBankTransfer_whenSave_thenPersistInDb() {
        final var entryDto = createEntryDto();
        entryDto.setPaymentType(PaymentType.BANK_TRANSFER);

        final var saved = entryService.save(entryDto);

        final var found = entryService.findById(saved.getId());
        assertEquals(PaymentType.BANK_TRANSFER, found.get().getPaymentType());
    }
}
