package pl.jurczakkamil.helper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.jurczakkamil.BaseDto;
import pl.jurczakkamil.Entry;
import pl.jurczakkamil.EntryDto;
import pl.jurczakkamil.consts.CurrencyType;
import pl.jurczakkamil.consts.EntryType;
import pl.jurczakkamil.consts.PaymentType;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NoArgsConstructor(access = AccessLevel.NONE)
public class EntryHelper {

    public final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    public static EntryDto createEntryDto() {
        return createEntryDto(true, true, true, true, true, true, true, true);
    }

    public static EntryDto createEntryDto(boolean withType, boolean withAccountId, boolean withAmount, boolean withCurrency,
                                          boolean withCategoryId, boolean withDate, boolean withDescription, boolean withPaymentType) {
        final var entryDto = new EntryDto();
        entryDto.setType(withType ? EntryType.EXPENSE : null);
        entryDto.setAccountId(withAccountId ? 10L : null);
        entryDto.setAmount(withAmount ? new BigDecimal("150") : null);
        entryDto.setCurrency(withCurrency ? CurrencyType.PLN : null);
        entryDto.setCategoryId(withCategoryId ? 10L: null);
        entryDto.setDate(withDate ? date() : null);
        entryDto.setDescription(withDescription ? "Entry generated from tests" : null);
        entryDto.setPaymentType(withPaymentType ? PaymentType.CASH : null);

        return entryDto;
    }

    public static List<EntryDto> createEntryDtos(int size) {
        final var dtos = new ArrayList<EntryDto>();
        IntStream.range(0, size)
                .forEach(i -> {
                    final var entryDto = createEntryDto();
                    dtos.add(entryDto);
                });
        return dtos;
    }

    public static Entry createEntry() {
        return createEntry(true, true, true, true, true, true, true, true);
    }

    public static Entry createEntry(boolean withType, boolean withAccountId, boolean withAmount, boolean withCurrency,
                                          boolean withCategoryId, boolean withDate, boolean withDescription, boolean withPaymentType) {
        final var entry = new Entry();
        entry.setType(withType ? EntryType.EXPENSE : null);
        entry.setAccountId(withAccountId ? 10L : null);
        entry.setAmount(withAmount ? new BigDecimal("150") : null);
        entry.setCurrency(withCurrency ? CurrencyType.PLN : null);
        entry.setCategoryId(withCategoryId ? 10L: null);
        entry.setDate(withDate ? date() : null);
        entry.setDescription(withDescription ? "Entry generated from tests" : null);
        entry.setPaymentType(withPaymentType ? PaymentType.CASH : null);

        return entry;
    }

    public static List<Long> extractIds(List<EntryDto> entries) {
        return entries.stream()
                .map(BaseDto::getId)
                .collect(Collectors.toList());
    }

    private static Date date() {
        Date date = null;
        try {
            date = dateFormat.parse("2021-09-02T21:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
