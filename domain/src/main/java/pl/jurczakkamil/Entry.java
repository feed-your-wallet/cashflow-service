package pl.jurczakkamil;

import lombok.Getter;
import lombok.Setter;
import pl.jurczakkamil.consts.CurrencyType;
import pl.jurczakkamil.consts.EntryType;
import pl.jurczakkamil.consts.PaymentType;
import pl.jurczakkamil.converter.CurrencyTypeConverter;
import pl.jurczakkamil.converter.EntryTypeConverter;
import pl.jurczakkamil.converter.PaymentTypeConverter;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
public class Entry extends BaseEntity {

    @Column(nullable = false)
    @Convert(converter = EntryTypeConverter.class)
    private EntryType type;

    @Column(nullable = false)
    private Long accountId;

    @Column(nullable = false)
    @Digits(integer = 9, fraction = 2, message = "Amount should has max 9 digits before comma and max 2 digits after comma")
    @DecimalMin(value = "0.0", inclusive = false, message = "Amount must be a greater than 0.0")
    private BigDecimal amount;

    @Column(nullable = false)
    @Convert(converter = CurrencyTypeConverter.class)
    private CurrencyType currency = CurrencyType.PLN;

    @Column(nullable = false)
    private Long categoryId;

    @Column(columnDefinition = "timestamp with time zone", nullable = false)
    private Date date;

    private String description;

    @Column(nullable = false)
    @Convert(converter = PaymentTypeConverter.class)
    private PaymentType paymentType = PaymentType.CASH;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        final Entry entry = (Entry) o;

        if (type != entry.type) return false;
        if (!accountId.equals(entry.accountId)) return false;
        if (!amount.equals(entry.amount)) return false;
        if (currency != entry.currency) return false;
        if (!categoryId.equals(entry.categoryId)) return false;
        if (!date.equals(entry.date)) return false;
        if (!Objects.equals(description, entry.description)) return false;
        return paymentType == entry.paymentType;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + accountId.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + currency.hashCode();
        result = 31 * result + categoryId.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + paymentType.hashCode();
        return result;
    }
}
