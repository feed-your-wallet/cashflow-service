package pl.jurczakkamil.converter;

import pl.jurczakkamil.consts.CurrencyType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class CurrencyTypeConverter implements AttributeConverter<CurrencyType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CurrencyType currencyType) {
        return currencyType.getIndex();
    }

    @Override
    public CurrencyType convertToEntityAttribute(Integer dbData) {
        return CurrencyType.byIndex(dbData);
    }
}
