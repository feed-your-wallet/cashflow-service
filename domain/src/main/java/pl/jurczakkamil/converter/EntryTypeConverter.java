package pl.jurczakkamil.converter;

import pl.jurczakkamil.consts.EntryType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EntryTypeConverter implements AttributeConverter<EntryType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EntryType entryType) {
        return entryType.getIndex();
    }

    @Override
    public EntryType convertToEntityAttribute(Integer dbData) {
        return EntryType.byIndex(dbData);
    }
}
