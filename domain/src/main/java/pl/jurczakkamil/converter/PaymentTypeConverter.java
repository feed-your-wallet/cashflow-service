package pl.jurczakkamil.converter;

import pl.jurczakkamil.consts.PaymentType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class PaymentTypeConverter implements AttributeConverter<PaymentType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(PaymentType paymentType) {
        return paymentType.getIndex();
    }

    @Override
    public PaymentType convertToEntityAttribute(Integer dbData) {
        return PaymentType.byIndex(dbData);
    }
}
