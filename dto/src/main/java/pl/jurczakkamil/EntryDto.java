package pl.jurczakkamil;

import lombok.*;
import pl.jurczakkamil.consts.CurrencyType;
import pl.jurczakkamil.consts.EntryType;
import pl.jurczakkamil.consts.PaymentType;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class EntryDto extends BaseDto {

    private EntryType type = EntryType.EXPENSE;
    private Long accountId;
    private BigDecimal amount;
    private CurrencyType currency = CurrencyType.PLN;
    private Long categoryId;
    private Date date;
    private String description;
    private PaymentType paymentType = PaymentType.CASH;
}
