package pl.jurczakkamil;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.service.cashflow.request.AddEntryRequestModel;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class RestApi extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration()
                .contextPath("{{server.servlet.context-path}}")
                .port("{{server.port}}")
                .enableCORS(true)
                .apiContextPath("{{server.servlet.context-path}}")
                .apiProperty("api.cashflow-management.title", "{{spring.application.name}}")
                .apiProperty("api.cashflow-management.version", "{{application.version}}")
                .apiContextRouteId("doc-cashflow-management-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/api")
                .get("/hello").consumes(APPLICATION_JSON)
                .to("direct:hello")
                .get("/entries").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .to("direct:get-all-entries")
                .post("/entries").consumes(APPLICATION_JSON).produces(APPLICATION_JSON)
                .type(AddEntryRequestModel.class)
                .to("direct:add-entry");
    }
}
