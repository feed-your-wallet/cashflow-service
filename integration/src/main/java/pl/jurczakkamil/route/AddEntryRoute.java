package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.EntryService;
import pl.jurczakkamil.mapper.EntryMapper;
import pl.jurczakkamil.service.cashflow.request.AddEntryRequestModel;

@Component
public class AddEntryRoute extends RouteBuilder {

    private final EntryService entryService;
    private final EntryMapper entryMapper;

    @Autowired
    public AddEntryRoute(EntryService entryService, EntryMapper entryMapper) {
        this.entryService = entryService;
        this.entryMapper = entryMapper;
    }

    @Override
    public void configure() {
        from("direct:add-entry").id("add-entry-route")
                .log(">>> ${body}")
                .process(this::saveEntry)
                .setBody(exchange -> "Successfully added entry");
    }

    private void saveEntry(Exchange exchange) {
        final var requestModel = exchange.getIn().getBody(AddEntryRequestModel.class);
        final var entryDto = entryMapper.toDto(requestModel);
        entryService.save(entryDto);
    }
}
