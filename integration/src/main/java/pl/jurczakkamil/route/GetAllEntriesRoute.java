package pl.jurczakkamil.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.jurczakkamil.EntryService;
import pl.jurczakkamil.mapper.EntryMapper;
import pl.jurczakkamil.model.EntryModel;

import java.util.List;

@Component
public class GetAllEntriesRoute extends RouteBuilder {

    private final EntryService entryService;
    private final EntryMapper entryMapper;

    @Autowired
    public GetAllEntriesRoute(EntryService entryService, EntryMapper entryMapper) {
        this.entryService = entryService;
        this.entryMapper = entryMapper;
    }

    @Override
    public void configure() {
        from("direct:get-all-entries").id("get-all-entries-route")
                .setBody(this::findAll);
    }

    private List<EntryModel> findAll(Exchange exchange) {
        final var entryDtos = entryService.findAll();
        return entryDtos.stream()
                .map(entryMapper::toCdmObject)
                .toList();
    }
}
