package pl.jurczakkamil.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class HelloRoute extends RouteBuilder {

    @Override
    public void configure() {
        from("direct:hello").id("hello-route")
                .transform().constant("Hello from CashFlow Management API");
    }
}
