package pl.jurczakkamil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jurczakkamil.mapper.EntryMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;

@Service
public class EntryService {

    private final EntryRepository entryRepository;
    private final EntryMapper entryMapper;

    @Autowired
    public EntryService(EntryRepository entryRepository, EntryMapper entryMapper) {
        this.entryRepository = entryRepository;
        this.entryMapper = entryMapper;
    }

    public EntryDto save(EntryDto dto) {
        requireNonNull(dto, "EntryDto cannot be null");
        final var entry = entryMapper.toEntity(dto);
        final var saved = entryRepository.save(entry);
        return entryMapper.toDto(saved);
    }

    public List<EntryDto> saveAll(List<EntryDto> dtos) {
        final var entries = dtos.stream()
                .map(entryMapper::toEntity)
                .toList();
        final var saved = entryRepository.saveAll(entries);
        return StreamSupport.stream(saved.spliterator(), false)
                .map(entryMapper::toDto)
                .toList();
    }

    public Optional<EntryDto> findById(Long id) {
        requireNonNull(id, "ID cannot be null");
        final var found = entryRepository.findById(id);

        if (found.isEmpty()) return Optional.empty();

        final var entry = found.get();
        final var dto = entryMapper.toDto(entry);

        return Optional.of(dto);
    }

    public List<EntryDto> findAll() {
        final var entriesIterable = entryRepository.findAll();
        return StreamSupport.stream(entriesIterable.spliterator(), false)
                .map(entryMapper::toDto)
                .toList();
    }
}
