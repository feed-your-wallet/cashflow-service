package pl.jurczakkamil.mapper;

import org.mapstruct.Mapper;
import pl.jurczakkamil.Entry;
import pl.jurczakkamil.EntryDto;
import pl.jurczakkamil.model.EntryModel;
import pl.jurczakkamil.service.cashflow.request.AddEntryRequestModel;

@Mapper(componentModel = "spring")
public interface EntryMapper {

    EntryDto toDto(Entry entry);

    Entry toEntity(EntryDto dto);

    EntryDto toDto(AddEntryRequestModel requestModel);

    EntryModel toCdmObject(EntryDto dto);
}
