package pl.jurczakkamil.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static java.util.Objects.requireNonNull;

@AllArgsConstructor
@Getter
public enum CurrencyType {

    PLN(1);

    private Integer index;

    public static CurrencyType byIndex(Integer index) {
        requireNonNull(index, "Index cannot be null");
        return Arrays.stream(CurrencyType.values())
                .filter(e -> e.getIndex().equals(index))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Cannot resolve CurrencyType by index: " + index));
    }
}
