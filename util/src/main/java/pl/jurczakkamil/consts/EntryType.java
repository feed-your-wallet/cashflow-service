package pl.jurczakkamil.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static java.util.Objects.requireNonNull;

@AllArgsConstructor
@Getter
public enum EntryType {

    EXPENSE(1),
    INCOME(2),
    TRANSFER(3);

    private Integer index;

    public static EntryType byIndex(Integer index) {
        requireNonNull(index, "Index cannot be null");
        return Arrays.stream(EntryType.values())
                .filter(e -> e.getIndex().equals(index))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Cannot resolve CurrencyType by index: " + index));
    }
}
